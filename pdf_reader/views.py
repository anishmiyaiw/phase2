import os
import io
import PyPDF2
import timeit
from django.shortcuts import render
from django.shortcuts import redirect
from .forms import PdfForm
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage


def index(request):
    if request.method == 'POST':
        Form = PdfForm(request.POST, request.FILES)
        if Form.is_valid():
            handle_uploaded_file(request.FILES['file_pdf'])
            return redirect('/text')
    else:
        Form = PdfForm()
        return render(request, "index.html", {'form': Form})


def pdf(request):
    pdfFileObj = open('media/test.pdf', 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    pageObj = pdfReader.getPage(0)
    a = pageObj.extractText()
    return render(request, "pfd.html", {'skill': a})


def text(request):
    pdf_path = 'media/ANISH.pdf'
    a = export_as_json(pdf_path)
    return render(request, "pfd.html", {'skill': a})


def handle_uploaded_file(f):
    with open('media/' + f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def extract_text_by_page(pdf_path):
    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh,
                                      caching=True,
                                      check_extractable=True):
            resource_manager = PDFResourceManager()
            fake_file_handle = io.StringIO()
            converter = TextConverter(resource_manager, fake_file_handle)
            page_interpreter = PDFPageInterpreter(resource_manager, converter)
            page_interpreter.process_page(page)

            text = fake_file_handle.getvalue()
            yield text
            converter.close()
            fake_file_handle.close()


def export_as_json(pdf_path):
    for page in extract_text_by_page(pdf_path):
        text = page[310:495]
        page = text
        return page
