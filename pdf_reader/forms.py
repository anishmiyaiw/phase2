from django import forms


class PdfForm(forms.Form):
    name = forms.CharField(label="Enter first name", max_length=50)
    file_pdf = forms.FileField()  # for creating file input  .FileField()
