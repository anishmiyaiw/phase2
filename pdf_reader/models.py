from django.urls import reverse
from django.db import models as models


class upload_file(models.Model):
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    file_pdf = models.FileField(upload_to='documents/')

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return u'%s' % self.pk

    def get_absolute_url(self):
        return reverse('pdf_reader_upload_file_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('pdf_reader_upload_file_update', args=(self.pk,))
