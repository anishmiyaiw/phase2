from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('skill', views.pdf, name='skill'),
    path('text', views.text, name='text'),

]
