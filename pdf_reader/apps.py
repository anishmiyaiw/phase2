from django.apps import AppConfig


class PdfReaderConfig(AppConfig):
    name = 'pdf_reader'
